package game

import game.graphics.Shader
import game.input.KeyboardInput
import game.level.Level
import org.joml.Matrix4f
import org.lwjgl.glfw.GLFW._
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL13._
import org.lwjgl.glfw.GLFWVidMode
import org.lwjgl.system.MemoryUtil.NULL
import org.lwjgl.opengl.GL


class GrowingSnake() extends Runnable {
  val width: Int = 1280
  val height: Int = 720
  val prMatrixHeight = 10.0f * 9.0f / 16.0f * 2


  var isRunning = false

  var window: Long = 0L
  var level: Level = null

  def start() = {
    isRunning = true
    val thread = new Thread(this, "Snake")
    thread.start()
  }

  def init(): Unit = {
    if (!glfwInit()) {
      throw new Exception("Unable to initialize GLFW")
    }

    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3)
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)

    window = glfwCreateWindow(width, height, "Snake", NULL, NULL)

    if (window == null) {
      throw new Exception("Could not create window.")
    }

    val vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor())

    val screenWidth = vidMode.width()
    val screenHeight = vidMode.height()
    glfwSetWindowPos(window, (screenWidth - width) / 2, (screenHeight - height) / 2)

    glfwSetKeyCallback(window, new KeyboardInput())

    glfwMakeContextCurrent(window)
    glfwSwapInterval(1)

    glfwShowWindow(window)
    GL.createCapabilities


    glEnable(GL_DEPTH_TEST)
    glActiveTexture(GL_TEXTURE1)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    println("OpenGL: " + glGetString(GL_VERSION))


    val prMatrix = new Matrix4f()
    prMatrix.setOrtho(-10.0f, 10.0f, -10.0f * 9.0f / 16.0f, 10.0f * 9.0f / 16.0f, -1.0f, 1.0f)

    Shader.BG.setUniforMat4f("pr_matrix", prMatrix)
    Shader.BG.setUniform1i("tex", 1)

    Shader.SNAKE.setUniforMat4f("pr_matrix", prMatrix)
    Shader.SNAKE.setUniform1i("tex", 1)

    Shader.TAIL.setUniforMat4f("pr_matrix", prMatrix)
    Shader.TAIL.setUniform1i("tex", 1)


    Shader.PIPE.setUniforMat4f("pr_matrix", prMatrix)
    Shader.PIPE.setUniform1i("tex", 1)


    level = new Level(prMatrixHeight)
  }

  override def run(): Unit = {
    init()

    var lastTime = System.currentTimeMillis()
    var secondsPassed = 0.0
    val ns = 1000000000 / 60.0
    var timer = System.currentTimeMillis()
    var updates = 0
    var frames = 0
    while (isRunning) {
      var now = System.nanoTime() // 28,972,027,914,521
      val delta = (now - lastTime) / ns
//      println(s"Now: $now, Delta: $delta")
      secondsPassed += delta
      lastTime = now
      if (secondsPassed >= 1.0) {
        update()
        updates += 1
        secondsPassed -= 1.0

      }

      render()

      frames += 1
      if (System.currentTimeMillis - timer > 1000) {
        timer += 1000
        println(s"$updates ups, $frames fps")
        updates = 0
        frames = 0
      }

      if (glfwWindowShouldClose(window))
        isRunning = false
    }

    glfwDestroyWindow(window)
    glfwTerminate()
  }

  def update(): Unit = {
    glfwPollEvents()
    level.update()

    if (level.isGameOver()) {
      level = new Level(prMatrixHeight)
    }

    // ESC closes the window
    if (KeyboardInput.keys(GLFW_KEY_ESCAPE)) {
      isRunning = false
    }
  }

  def render(): Unit = {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    level.render()

    val error = glGetError()
    if (error != GL_NO_ERROR) {
      println(error)
    }

    glfwSwapBuffers(window)

  }


}

object GrowingSnake {
  def main(args: Array[String]): Unit = {
    println("Starting Snake. We all Love no'omi Blitz. Right?")
    new GrowingSnake().start()
  }
}
