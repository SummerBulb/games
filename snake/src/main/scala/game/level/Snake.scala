package game.level

import org.lwjgl.glfw.GLFW._

import game.graphics.{Shader, Texture, VertexArray}
import game.input.KeyboardInput
import org.joml.{Matrix4f, Vector3f}

class Snake() {
  val SIZE: Float = 1.0f

  var rot = 0.0f
  var alive = true

  var direction = new Vector3f(0.1f, 0.0f, 0.0f)

  var position = new Vector3f()

  var up = new Vector3f(0.0f, 0.1f, 0f)
  var down = new Vector3f(0.0f, -0.1f, 0f)
  var right = new Vector3f(0.1f, 0.0f, 0.0f)
  var left = new Vector3f(-0.1f, 0.0f, 0.0f)

  val tail = (1 to 10).map((i: Int) => new TailPart(new Vector3f(-0.5f * i, 0f, 0f)))

  val vertices = Array[Float](
    -SIZE / 2.0f, -SIZE / 2.0f, 0.2f,
    -SIZE / 2.0f, SIZE / 2.0f, 0.2f,
    SIZE / 2.0f, SIZE / 2.0f, 0.2f,
    SIZE / 2.0f, -SIZE / 2.0f, 0.2f
  )

  val indices = Array[Byte](
    0, 1, 2,
    2, 3, 0

  )


  val textureCoordinates = Array[Float](
    0, 1,
    0, 0,
    1, 0,
    1, 1
  )

  val mesh = new VertexArray(vertices, indices, textureCoordinates)

  val texture = new Texture("/assets/snakeHead.png")

  def update() = {
    if (alive) {
      val prevPosition = new Vector3f(position)
      /* position.y -= delta

     if (KeyboardInput.isKeyDown(GLFW_KEY_SPACE)) {
       delta = -0.15f
     }
     else {
       delta += 0.01f
     }

     rot = -delta * 90.0f*/
      /*
        if (position.x != target.x && position.y != target.y)
          throw new Exception("Illegal state. The tail location must be on the same axis (Y or X) as the target")
    */

      if (KeyboardInput.isKeyDown(GLFW_KEY_UP) && direction != down) {
        direction = up
      }

      if (KeyboardInput.isKeyDown(GLFW_KEY_DOWN) && direction != up) {
        direction = down
      }

      if (KeyboardInput.isKeyDown(GLFW_KEY_RIGHT) && direction != left) {
        direction = right
      }

      if (KeyboardInput.isKeyDown(GLFW_KEY_LEFT) && direction != right) {
        direction = left
      }

      position.add(direction)

      tail.foreach((part: TailPart) => {
        part.waypoint(prevPosition)
        part.update()
      })
    }
  }


  def kill() = {
    alive = false

  }

  def render() = {
    Shader.SNAKE.enable()
    val angle = Math.toRadians(rot).toFloat

    Shader.SNAKE.setUniforMat4f("ml_matrix", new Matrix4f().translate(position).rotate(angle, new Vector3f(0.0f, 0.0f, 1.0f)))
    texture.bind()
    mesh.render()
    Shader.SNAKE.disable()

    tail.foreach((part: TailPart) => part.render())
  }

}
