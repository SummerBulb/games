package game.level

import game.graphics.{Shader, Texture, VertexArray}
import game.input.KeyboardInput
import org.joml.Matrix4f
import org.lwjgl.glfw.GLFW._

import scala.util.Random


class Level(height: Float) {

  var xScroll = 0
  private var map = 0
  private var index = 0
  private val OFFSET = 5.0f
  private var control = true
  private var reset = false
  private var time = 0.0f

  val vertices = Array[Float](
    -10.0f, -10.0f * 9.0f / 16.0f, 0.0f,
    -10.0f, 10.0f * 9.0f / 16.0f, 0.0f,
    10.0f, 10.0f * 9.0f / 16.0f, 0.0f,
    10.0f, -10.0f * 9.0f / 16.0f, 0.0f
  )

  val indices = Array[Byte](
    0, 1, 2,
    2, 3, 0

  )

  val textureCoordinates = Array[Float](
    0, 1,
    0, 0,
    1, 0,
    1, 1
  )

  val fade = new VertexArray(6)
  val background = new VertexArray(vertices, indices, textureCoordinates)
  val bgTexture = new Texture("/assets/bg.jpeg")
  val snake = new Snake()


  def isGameOver() = {
    reset
  }


  def update() = {

    snake.update


    if (collision) {
     snake.kill()
      reset = true
    }

    limitSnake()


  }

  def limitSnake() = {
    if ((snake.position.x - snake.SIZE / 2) <= -10.0f)
      snake.position.x = -10.0f + snake.SIZE / 2
    if ((snake.position.x + snake.SIZE / 2) >= 10.0f)
      snake.position.x = 10.0f - snake.SIZE / 2
    if ((snake.position.y - snake.SIZE / 2) <= -10.0f * 9.0f / 16.0f)
      snake.position.y = -10.0f * 9.0f / 16.0f + snake.SIZE / 2
    if ((snake.position.y + snake.SIZE / 2) >= 10.0f * 9.0f / 16.0)
      snake.position.y = 10.0f * 9.0f / 16.0f - snake.SIZE / 2

  }

  def collision(): Boolean = {
    (snake.position.x - snake.SIZE / 2) <= -10.0f ||
      (snake.position.x + snake.SIZE / 2) >= 10.0f ||
      (snake.position.y - snake.SIZE / 2) <= -10.0f * 9.0f / 16.0f ||
      (snake.position.y + snake.SIZE / 2) >= 10.0f * 9.0f / 16.0
  }


  def render() = {

    bgTexture.bind()
    Shader.BG.enable()
    background.bind()
    Shader.BG.setUniforMat4f("vw_matrix", new Matrix4f().translate(0.0f, 0.0f, 0.0f))

    background.draw()
    Shader.BG.disable()
    bgTexture.unbind()

    snake.render()

  }

}
