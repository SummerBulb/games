package game.level

import game.graphics.{Texture, VertexArray}
import org.joml.{Matrix4f, Vector3f}

class Pipe(val x: Float, val y: Float) {

  var position = new Vector3f()
  position.x = x
  position.y = y

  val ml_matrix = new Matrix4f().translate(position)


}

object Pipe {

  val width = 1.5f
  val height = 8.0f

  lazy val (mesh, texture) = create()

  def create() = {

    val vertices = Array[Float](
      0.0f, 0.0f, 0.1f,
      0.0f, height, 0.1f,
      width, height, 0.1f,
      width, 0.0f, 0.1f,
    )

    val indices = Array[Byte](
      0, 1, 2,
      2, 3, 0

    )

    val textureCoordinates = Array[Float](
      0, 1,
      0, 0,
      1, 0,
      1, 1
    )

    val mesh = new VertexArray(vertices, indices, textureCoordinates)
    val texture = new Texture("/assets/pipe.png")

    (mesh, texture)
  }


}
