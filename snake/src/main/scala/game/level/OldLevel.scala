package game.level

import game.graphics.{Shader, Texture, VertexArray}
import game.input.KeyboardInput
import org.joml.Matrix4f
import org.lwjgl.glfw.GLFW._

import org.lwjgl.opengl.GL11.glGetError
import org.lwjgl.opengl.GL11.GL_NO_ERROR

import scala.util.Random


class OldLevel(height:Float) {

  var xScroll = 0
  private var map = 0
  private var index = 0
  private val OFFSET = 5.0f
  private var control = true
  private var reset = false
  private var time = 0.0f

  val pipes: Array[Pipe] = createPipes()

  val vertices = Array[Float](
    -10.0f, -10.0f * 9.0f / 16.0f, 0.0f,
    -10.0f, 10.0f * 9.0f / 16.0f, 0.0f,
    0.0f, 10.0f * 9.0f / 16.0f, 0.0f,
    0.0f, -10.0f * 9.0f / 16.0f, 0.0f
  )

  val indices = Array[Byte](
    0, 1, 2,
    2, 3, 0

  )

  val textureCoordinates = Array[Float](
    0, 1,
    0, 0,
    1, 0,
    1, 1
  )

  val fade = new VertexArray(6)
  val background = new VertexArray(vertices, indices, textureCoordinates)
  val bgTexture = new Texture("/assets/bg.jpeg")
  val bird = new Snake()

  def createPipes() = {
    (0 until 5 * 2 by 2).flatMap((i: Int) => {
      val pipe = new Pipe(OFFSET + i * 3.0f, Random.nextFloat() * 4.0f)
      val pipe2 = new Pipe(pipe.x, pipe.y - 11.5f)
      index += 2
      Seq(pipe, pipe2)
    }).toArray
  }

  def updatePipes() = {
    pipes(index % 10) = new Pipe(OFFSET + index * 3.0f, Random.nextFloat() * 4.0f)
    pipes((index + 1) % 10) = new Pipe(pipes(index % 10).x, pipes(index % 10).y - 11.5f)
    index += 2
  }

  def isGameOver() = {
    reset
  }


  def update() = {
    if (control) {
      xScroll -= 1

      if (-xScroll % 335 == 0)
        map += 1
      if (-xScroll > 250 && -xScroll % 120 == 0) {

        updatePipes()
      }
    }
    bird.update()

    if (control && collision) {
      control = false
      //bird.fall()
//      println("Collision!")
    }

    if (!control && KeyboardInput.isKeyDown(GLFW_KEY_SPACE)) {
      reset = true
    }

    time += 0.01f

  }

  private def renderPipes() = {
    Shader.PIPE.enable()
    Shader.PIPE.setUniform2f("bird", 0, bird.position.y)
    Shader.PIPE.setUniforMat4f("vw_matrix", new Matrix4f().translate(xScroll * 0.05f, 0.0f, 0.0f))
    Pipe.texture.bind()
    Pipe.mesh.bind()

    for (i <- 0 until 5 * 2) {
      Shader.PIPE.setUniforMat4f("ml_matrix", pipes(i).ml_matrix)
      Shader.PIPE.setUniform1i("top", 1 - i % 2)
      Pipe.mesh.draw()
    }
    Pipe.texture.unbind()
    Pipe.mesh.unbind()


  }


  def collision(): Boolean = {

//    println(bird.position.y)

    bird.position.y < -height/2 ||
    bird.position.y > height/2 ||
    (0 until 2 * 5).exists((i: Int) => {
      val bx = -xScroll * 0.05f
      val by = bird.position.y
      val px = pipes(i).position.x
      val py = pipes(i).position.y

      val bx0 = bx - bird.SIZE / 2.0f
      val bx1 = bx + bird.SIZE / 2.0f
      val by0 = by - bird.SIZE / 2.0f
      val by1 = by + bird.SIZE / 2.0f

      val px0 = px
      val px1 = px + Pipe.width
      val py0 = py
      val py1 = py + Pipe.height


      (bx1 > px0 && bx0 < px1) &&
        (by1 > py0 && by0 < py1)

    })
  }

  def render() = {

    bgTexture.bind()
    Shader.BG.enable()
    Shader.BG.setUniform2f("bird", 0, bird.position.y)
    background.bind()
    for (i <- map until map + 4) {
      Shader.BG.setUniforMat4f("vw_matrix", new Matrix4f().translate(i * 10 + xScroll * 0.03f, 0.0f, 0.0f))
      background.draw()
    }
    Shader.BG.disable()
    bgTexture.unbind()

    renderPipes()
    bird.render()

    Shader.FADE.enable()
    Shader.FADE.setUniform1f("time", time)
    fade.render()
    Shader.FADE.disable()
  }

}
