package game.level

import game.graphics.{Shader, Texture, VertexArray}
import game.input.KeyboardInput
import org.joml.{Matrix4f, Vector3f}
import org.lwjgl.glfw.GLFW._

import scala.collection.mutable

class TailPart(startPosition:Vector3f) {


  val SIZE:Float = 0.5f

  var rot = 0.0f
  var delta = 0.0f

  var direction = new Vector3f(0.1f,0.0f,0.0f)

  var position = startPosition
  val speed = 0.1f

  val waypoints = mutable.Queue[Vector3f]()


  val vertices = Array[Float](
    -SIZE / 2.0f, -SIZE / 2.0f, 0.2f,
    -SIZE / 2.0f, SIZE / 2.0f, 0.2f,
    SIZE / 2.0f, SIZE / 2.0f, 0.2f,
    SIZE / 2.0f, -SIZE / 2.0f, 0.2f
  )

  val indices = Array[Byte](
    0, 1, 2,
    2, 3, 0

  )



  val textureCoordinates = Array[Float](
    0, 1,
    0, 0,
    1, 0,
    1, 1
  )

  val mesh = new VertexArray(vertices, indices, textureCoordinates)

  val texture = new Texture("/assets/tailPart.png")

  def waypoint(position: Vector3f) = {
    if (waypoints.isEmpty || position != waypoints.front)
      waypoints.enqueue(position)

  }

  def update() = {
    println(waypoints.length)

    val target = waypoints.front

/*
    if (position.x != target.x && position.y != target.y)
      throw new Exception("Illegal state. The tail location must be on the same axis (Y or X) as the target")
*/

    if (position.y == target.y) {
      val xDistance = target.x - position.x

      if (Math.abs(xDistance)  <= speed) {
        position.x += xDistance
      } else {
        position.x += speed * Math.signum(xDistance)
      }
    } else {
      val yDistance = target.y - position.y
      if (Math.abs(yDistance) <= speed) {
        position.y += yDistance
      } else {
        position.y += speed * Math.signum(yDistance)
      }
    }

    if (position == waypoints.front) {
      waypoints.dequeue()
    }




    //advance


    //normalize

   /* position.y -= delta

    if (KeyboardInput.isKeyDown(GLFW_KEY_SPACE)) {
      delta = -0.15f
    }
    else {
      delta += 0.01f
    }

    rot = -delta * 90.0f*/
//
//    if (KeyboardInput.isKeyDown(GLFW_KEY_UP) && direction != down) {
//      direction = up
//    }
//
//    if (KeyboardInput.isKeyDown(GLFW_KEY_DOWN) && direction != up) {
//      direction = down
//    }
//
//    if (KeyboardInput.isKeyDown(GLFW_KEY_RIGHT) && direction != left) {
//      direction = right
//    }
//
//    if (KeyboardInput.isKeyDown(GLFW_KEY_LEFT) && direction != right) {
//      direction = left
//    }
//
//    position.add(direction)
  }



  def render() = {
    Shader.TAIL.enable()

    Shader.TAIL.setUniforMat4f("ml_matrix", new Matrix4f().translate(position))
    texture.bind()
    mesh.render()
    Shader.TAIL.disable()
  }

}
