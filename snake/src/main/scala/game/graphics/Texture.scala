package game.graphics

import java.io.FileInputStream

import org.lwjgl.opengl.GL11._
import javax.imageio.ImageIO
import org.lwjgl.BufferUtils

class Texture(val path: String) {


  val (texture, a, b): (Int, Int, Int) = load(path)

  def load(path: String) = {
    val (pixels: Array[Int], width: Int, height: Int) =
      try {
        val image = ImageIO.read(this.getClass.getResource(path).openStream())
        val width = image.getWidth
        val height = image.getHeight

        val pixels = image.getRGB(0, 0, width, height, null, 0, width)

        (pixels, width, height)
      } catch {
        case e: Exception => e.printStackTrace()
      }

    val fixedPixels = pixels.map((pixel: Int) => {
      val a = (pixel & 0xff000000) >> 24
      val r = (pixel & 0xff0000) >> 16
      val g = (pixel & 0xff00) >> 8
      val b = (pixel & 0xff) >> 0

      a << 24 | b << 16 | g << 8 | r
    })

    val textureId = glGenTextures()
    glBindTexture(GL_TEXTURE_2D, textureId)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    val buffer = BufferUtils.createIntBuffer(fixedPixels.length)
    buffer.put(fixedPixels).flip()
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)

    glBindTexture(GL_TEXTURE_2D, 0)

    (textureId, width, height)
  }

  def bind() = {
    glBindTexture(GL_TEXTURE_2D, texture)
  }

  def unbind() = {
    glBindTexture(GL_TEXTURE_2D, 0)
  }
}
