package game.graphics

import java.nio.FloatBuffer

import game.graphics.Shader.{BG, SNAKE, PIPE}
import org.lwjgl.opengl.GL20._
import game.utils.ShaderUtils
import org.joml.{Matrix4f, Vector3f}
import org.lwjgl.BufferUtils

import scala.collection.mutable

class Shader(vertex: String, fragment: String) {


  val ID = ShaderUtils.load(vertex, fragment)


  var enabled = false
  val cache = mutable.Map[String, Int]()

  def getUniform(name: String) = {

    if (cache.contains(name))
      cache(name)
    else {

      val location = glGetUniformLocation(ID, name)
      if (location == -1) {
        System.err.println("Could not find uniform variable '" + name + "'!")
        System.err.println(Thread.currentThread.getStackTrace.mkString("\n"))
      } else {
        cache.put(name, location)
      }
      location
    }
  }

  def setUniform1i(name: String, value: Int) = {
    if (!enabled) enable()
    glUniform1i(getUniform(name), value)
  }

  def setUniform1f(name: String, value: Float) = {
    if (!enabled) enable()
    glUniform1f(getUniform(name), value)
  }

  def setUniform2f(name: String, x: Float, y: Float) = {
    if (!enabled) enable()
    glUniform2f(getUniform(name), x, y)
  }

  def setUniform3f(name: String, vector: Vector3f) = {
    if (!enabled) enable()
    glUniform3f(getUniform(name), vector.x, vector.y, vector.z)
  }

  def setUniforMat4f(name: String, matrix: Matrix4f) = {
    if (!enabled) enable()

    val buffer = BufferUtils.createFloatBuffer(16)
    glUniformMatrix4fv(getUniform(name), false, matrix.get(buffer))
  }

  def enable() = {
    glUseProgram(ID)
    enabled = true
  }

  def disable() = {
    glUseProgram(0)
    enabled = false

  }

}

object Shader {
  val VERTEX_ATTRIB = 0
  val TCOORD_ATTRIB = 1

//  val shadersPath = "/home/shmuel/dev/games/flappy/shaders/"
  val shadersPath = "/shaders/"
  lazy val BG = new Shader(shadersPath + "bg.vert", shadersPath + "bg.frag")
  lazy val SNAKE = new Shader(shadersPath + "snake.vert", shadersPath + "snake.frag")
  lazy val TAIL = new Shader(shadersPath + "tail.vert", shadersPath + "tail.frag")
  lazy val PIPE = new Shader(shadersPath + "pipe.vert", shadersPath + "pipe.frag")
  lazy val FADE = new Shader(shadersPath + "fade.vert", shadersPath + "fade.frag")


  def loadAll() = {

  }

}