package game.utils


import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.IntBuffer


object BufferUtil {
  def createByteBuffer(array: Array[Byte]): ByteBuffer = {
    val result = ByteBuffer.allocateDirect(array.length).order(ByteOrder.nativeOrder)
    result.put(array).flip
    result
  }

  def createFloatBuffer(array: Array[Float]): FloatBuffer = {
    val result = ByteBuffer.allocateDirect(array.length << 2).order(ByteOrder.nativeOrder).asFloatBuffer
    result.put(array).flip
    result
  }

  def createIntBuffer(array: Array[Int]): IntBuffer = {
    val result = ByteBuffer.allocateDirect(array.length << 2).order(ByteOrder.nativeOrder).asIntBuffer
    result.put(array).flip
    result
  }
}
