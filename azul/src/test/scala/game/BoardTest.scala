package game

import org.scalatest.FunSuite

class BoardTest extends FunSuite {

  test("testTileAndScore") {
    val boardForScoring = new Board()
      .place("R", 1, 1)
      .place("B", 2, 2)
      .place("Y", 3, 3)
      .place("G", 4, 4)
      .place("A", 5, 5)
    println(boardForScoring)

    val board = boardForScoring.tileAndScore(1)
    println(board)

    assert( board.score == 1)

  }

}
