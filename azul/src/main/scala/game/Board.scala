package game

import utils.StringUtil

class Board(val score: Int, wall: Array[Array[String]], prepWall: Array[Array[String]], floor: Int) {

  def this() = {
    this(0,
      Array.ofDim[String](5, 5),
      (1 to 5).map((i: Int) => Array[String]()).toArray,
      0
    )
  }

  def place(colour: String, count: Int, line: Int): Board = {
    if (line > 5 || line < 1)
      throw new Exception("Line number must be between 1 and 5.")
    else {
      if (!prepWall(line - 1).isEmpty && !prepWall(line - 1).contains(colour))
        throw new Exception("A tile cannot be place in a line that already has tiles with a different colour.")
      else {
        if (wall(line - 1).contains(colour))
          throw new Exception("A tile cannot be placed in a line if same line on the wall already has a tile with that colour.")
        else {
          val array: Array[String] = (1 to count).map(_ => colour).toArray
          val strings: Array[String] = prepWall(line - 1)
          val newLineTry: Array[String] = strings ++ array
          val newFloor: Int = floor + newLineTry.length - line

          new Board(
            score,
            wall,
            prepWall.updated(line - 1, newLineTry.take(line)),
            newFloor
          )
        }
      }
    }
  }

  def tileAndScore(line: Int): Board = {
    if (prepWall(line - 1).size == line) {
      val colour = prepWall(line - 1).head
      val wallLine = wall(line - 1)
      val tileIndex = fixedWall(line - 1).indexOf(colour)
      val sizeLeft =
        if (tileIndex > 0)
          (tileIndex - 1 to 1).takeWhile((i: Int) => wallLine(i - 1) != null).size
        else
          0
      val sizeRight =
        if (tileIndex < 5)
          (tileIndex + 1 to 5).takeWhile((i: Int) => wallLine(i - 1) != null).size
        else
          0
      val rowScore = {
        {
          if (sizeLeft > 0) sizeLeft + 1 else 0
        }
        + {
          if (sizeRight > 0) sizeRight else 0
        }
      }

      val sizeUp =
        if (line-1 > 0)
          (line -1 - 1 to 1).takeWhile((i: Int) => wall(i - 1)(tileIndex) != null).size
        else
          0
      val sizeDown =
        if (line - 1 < 5)
          (line -1 + 1 to 5).takeWhile((i: Int) => wall(i - 1)(tileIndex) != null).size
        else
          0
      val colScore = {
        {
          if (sizeUp > 0) sizeUp + 1 else 0
        }
        + {
          if (sizeDown > 0) sizeDown else 0
        }
      }

      val tileScore = {
        if (rowScore > 0 || colScore > 0)
          rowScore + colScore
        else
          1
      }

      new Board(
        score + tileScore,
        wall.updated(line - 1, wall(line - 1).updated(tileIndex, colour)),
        prepWall.updated(line - 1, Array[String]()),
        floor
      )
    }
    else
      this
  }

  private val fixedWall = Array(
    Array("R", "G", "Y", "B", "A"),
    Array("A", "R", "G", "Y", "B"),
    Array("B", "A", "R", "G", "Y"),
    Array("Y", "B", "A", "R", "G"),
    Array("G", "Y", "B", "A", "R"),
  )


  override def toString(): String = {
    val scoreStr = "Score: " + score + "\n"

    val wallPrepStr = prepWall.map((lines: Array[String]) => {
      val lineStr = lines.toSeq.mkString(" | ")
      StringUtil.padLeft(lineStr, 17 - lineStr.length)
    })
    val wallStr = wall.map((lines: Array[String]) => lines.mkString(" | "))


    val wallTable = wallPrepStr
      .zip(wallStr)
      .map((tuple: (String, String)) => tuple._1 + "  ||  " + tuple._2).mkString("\n")

    scoreStr +
      wallTable + "\n" +
      "Floor: " + floor


  }
}
