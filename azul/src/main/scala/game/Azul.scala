package game

object Azul {
  val asciiArt =
    """   _____               .__
      |  /  _  \ __________ __|  |
      | /  /_\  \\___   /  |  \  |
      |/    |    \/    /|  |  /  |__
      |\____|__  /_____ \____/|____/
      |        \/      \/           """.stripMargin

  def main(args: Array[String]): Unit = {
    println(asciiArt)


    val board = new Board(
      0,
      Array(
        Array("E", "E", "E", "E", "E"),
        Array("E", "E", "E", "E", "E"),
        Array("E", "E", "E", "E", "E"),
        Array("E", "E", "E", "E", "E"),
        Array("E", "E", "E", "E", "E")
      ),
      Array(
        Array("E"),
        Array("E", "E"),
        Array("E", "E", "E"),
        Array("E", "E", "E", "E"),
        Array("E", "E", "E", "E", "E")
      ),
      0
    )
    //    println(board)

    val board1 = new Board()
      .place("R", 1, 1)
      .place("B", 2, 2)
      .place("Y", 3, 3)
      .place("G", 4, 4)
      .place("A", 5, 5)
    println(board1)

    println(board1.tileAndScore(1))


  }


}
