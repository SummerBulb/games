package utils

object StringUtil {

  def padLeft(s: String, pad: Int, char: String = " "): String = {
    char * pad + s
  }
}
