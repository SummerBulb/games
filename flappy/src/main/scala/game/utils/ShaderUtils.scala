package game.utils

import java.io.InputStream

import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL20._

import scala.io.Source

object ShaderUtils {

  def load(vertPath: String, fragPath: String): Int = {
    val vert: String = Source.fromInputStream(getClass.getResourceAsStream(vertPath)).mkString   //   File( this.getClass.getResource(vertPath).toURI.getPath).mkString
    val frag: String = Source.fromInputStream(getClass.getResourceAsStream(fragPath)).mkString  // File(this.getClass.getResource(fragPath).toURI.getPath).mkString

    create(vert, frag)
  }


  def create(vert: String, frag: String): Int = {
    val program:Int = glCreateProgram()
    val vertID = glCreateShader(GL_VERTEX_SHADER)
    val fragID = glCreateShader(GL_FRAGMENT_SHADER)

    glShaderSource(vertID, vert)
    glShaderSource(fragID, frag)

    glCompileShader(vertID)
    if (glGetShaderi(vertID, GL_COMPILE_STATUS) != GL_TRUE) {
      val infoLog = glGetShaderInfoLog(vertID)
      throw new Exception("Failed to compile vertex shader.\nDetails:\n" + infoLog)
    }

    glCompileShader(fragID)
    if (glGetShaderi(fragID, GL_COMPILE_STATUS) != GL_TRUE) {
      val infoLog = glGetShaderInfoLog(fragID)
      throw new Exception("Failed to compile fragment shader.\nDetails:\n" + infoLog)
    }

    glAttachShader(program, vertID)
    glAttachShader(program, fragID)
    glLinkProgram(program)


    glValidateProgram(program)

    glDeleteShader(vertID)
    glDeleteShader(fragID)

    val error = glGetError()
    if (error != GL_NO_ERROR) {
      println(error)
      System.exit(200)
    }


    program
  }

}
