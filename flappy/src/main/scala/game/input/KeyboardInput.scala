package game.input

import org.lwjgl.glfw.{GLFW, GLFWKeyCallback}

class KeyboardInput extends GLFWKeyCallback{



  override def invoke(window: Long, key: Int, scancode: Int, action: Int, mods: Int): Unit = {
    KeyboardInput.keys(key) = action != GLFW.GLFW_RELEASE
  }
}


object KeyboardInput {
  val keys:Array[Boolean] = new Array[Boolean](65536)

  def isKeyDown(keyCode: Int) = {
    keys(keyCode)
  }
}
