package game.level

import org.lwjgl.glfw.GLFW._

import game.graphics.{Shader, Texture, VertexArray}
import game.input.KeyboardInput
import org.joml.{Matrix4f, Vector3f}

class Bird() {
  val SIZE = 1.0f

  var rot = 0.0f
  var delta = 0.0f

  var position = new Vector3f()


  val vertices = Array[Float](
    -SIZE / 2.0f, -SIZE / 2.0f, 0.2f,
    -SIZE / 2.0f, SIZE / 2.0f, 0.2f,
    SIZE / 2.0f, SIZE / 2.0f, 0.2f,
    SIZE / 2.0f, -SIZE / 2.0f, 0.2f
  )

  val indices = Array[Byte](
    0, 1, 2,
    2, 3, 0

  )



  val textureCoordinates = Array[Float](
    0, 1,
    0, 0,
    1, 0,
    1, 1
  )

  val mesh = new VertexArray(vertices, indices, textureCoordinates)

  val texture = new Texture("/assets/bird.png")

  def update() = {
    position.y -= delta

    if (KeyboardInput.isKeyDown(GLFW_KEY_SPACE)) {
      delta = -0.15f
    }
    else {
      delta += 0.01f
    }

    rot = -delta * 90.0f
  }


  def fall() = {
    delta = -0.15f

  }

  def render() = {
    Shader.BIRD.enable()
    val angle = Math.toRadians(rot).toFloat

    Shader.BIRD.setUniforMat4f("ml_matrix", new Matrix4f().translate(position).rotate(angle, new Vector3f(0.0f,0.0f,1.0f)))
    texture.bind()
    mesh.render()
    Shader.BIRD.disable()
  }

}
