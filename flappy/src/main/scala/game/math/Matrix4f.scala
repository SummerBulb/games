package game.math

import java.nio.FloatBuffer

import game.utils.BufferUtil
import org.joml.Vector3f


object Matrix4f {
  val SIZE: Int = 4 * 4

  def identity: Matrix4f = {
    val result = new Matrix4f
    var i = 0
    while ( {
      i < SIZE
    }) {
      result.elements(i) = 0.0f

      {
        i += 1; i - 1
      }
    }
    result.elements(0 + 0 * 4) = 1.0f
    result.elements(1 + 1 * 4) = 1.0f
    result.elements(2 + 2 * 4) = 1.0f
    result.elements(3 + 3 * 4) = 1.0f
    result
  }

  def orthographic(left: Float, right: Float, bottom: Float, top: Float, near: Float, far: Float): Matrix4f = {
    val result = identity
    result.elements(0 + 0 * 4) = 2.0f / (right - left)
    result.elements(1 + 1 * 4) = 2.0f / (top - bottom)
    result.elements(2 + 2 * 4) = 2.0f / (near - far)
    result.elements(0 + 3 * 4) = (left + right) / (left - right)
    result.elements(1 + 3 * 4) = (bottom + top) / (bottom - top)
    result.elements(2 + 3 * 4) = (far + near) / (far - near)
    result
  }

  def translate(vector: Vector3f): Matrix4f = {
    val result = identity
    result.elements(0 + 3 * 4) = vector.x
    result.elements(1 + 3 * 4) = vector.y
    result.elements(2 + 3 * 4) = vector.z
    result
  }

  def rotate(angle: Float): Matrix4f = {
    val result = identity
    val r = Math.toRadians(angle).toFloat
    val cos = Math.cos(r).toFloat
    val sin = Math.sin(r).toFloat
    result.elements(0 + 0 * 4) = cos
    result.elements(1 + 0 * 4) = sin
    result.elements(0 + 1 * 4) = -sin
    result.elements(1 + 1 * 4) = cos
    result
  }
}

class Matrix4f() {
  var elements = new Array[Float](Matrix4f.SIZE)

  def multiply(matrix: Matrix4f): Matrix4f = {
    val result = new Matrix4f
    var y = 0
    while ( {
      y < 4
    }) {
      var x = 0
      while ( {
        x < 4
      }) {
        var sum = 0.0f
        var e = 0
        while ( {
          e < 4
        }) {
          sum += this.elements(x + e * 4) * matrix.elements(e + y * 4)

          {
            e += 1; e - 1
          }
        }
        result.elements(x + y * 4) = sum

        {
          x += 1; x - 1
        }
      }

      {
        y += 1; y - 1
      }
    }
    result
  }

  def toFloatBuffer: FloatBuffer = BufferUtil.createFloatBuffer(elements)
}