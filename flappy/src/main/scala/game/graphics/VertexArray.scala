package game.graphics

import java.nio.{ByteBuffer, ByteOrder, FloatBuffer, IntBuffer}

import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20._
import org.lwjgl.opengl.GL30._

class VertexArray(vertices: Array[Float], indices: Array[Byte], textureCoordinates: Array[Float], indicesCount:Option[Int]=None) {

  val count = indicesCount.getOrElse(indices.length)

  val vao = glGenVertexArrays()

  def this(count: Int) = {
    this(Array(), Array(), Array(), Some(count))
  }

  val ibo =
    if (vertices != null && vertices.length > 0) {
      glBindVertexArray(vao)


      val vbo = glGenBuffers()
      glBindBuffer(GL_ARRAY_BUFFER, vbo)
      //  val verticesBuffer = BufferUtils.createFloatBuffer(vertices.length)
      //  verticesBuffer.put(vertices).flip()
      glBufferData(GL_ARRAY_BUFFER, createFloatBuffer(vertices), GL_STATIC_DRAW)
      glVertexAttribPointer(Shader.VERTEX_ATTRIB, 3, GL_FLOAT, false, 0, 0)
      glEnableVertexAttribArray(Shader.VERTEX_ATTRIB)


      val tbo = glGenBuffers()
      glBindBuffer(GL_ARRAY_BUFFER, tbo)
      //  val texCoordBuffer = BufferUtils.createFloatBuffer(textureCoordinates.length)
      //  texCoordBuffer.put(textureCoordinates).flip()
      glBufferData(GL_ARRAY_BUFFER, createFloatBuffer(textureCoordinates), GL_STATIC_DRAW)
      glVertexAttribPointer(Shader.TCOORD_ATTRIB, 2, GL_FLOAT, false, 0, 0)
      glEnableVertexAttribArray(Shader.TCOORD_ATTRIB)


      val ibo = glGenBuffers()
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo)
      //  private val indicesBuffer: ByteBuffer = BufferUtils.createByteBuffer(indices.length)
      //  indicesBuffer.put(indices).flip()
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, createByteBuffer(indices), GL_STATIC_DRAW)

      //  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
      glBindBuffer(GL_ARRAY_BUFFER, 0)
      glBindVertexArray(0)
      ibo
    } else {
      -1
    }

  def bind() = {
    glBindVertexArray(vao)
    if (ibo > 0)
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo)
  }

  def unbind() = {
    if (ibo > 0)
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
  }

  def draw() = {
    if (ibo > 0)
      glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_BYTE, 0)
    else {
      glDrawArrays(GL_TRIANGLES, 0, count)
    }

  }


  def render() = {
    bind()
    draw()
  }


  def createByteBuffer(array: Array[Byte]): ByteBuffer = {
    val result = ByteBuffer.allocateDirect(array.length).order(ByteOrder.nativeOrder)
    result.put(array).flip
    result
  }

  def createFloatBuffer(array: Array[Float]): FloatBuffer = {
    val result = ByteBuffer.allocateDirect(array.length << 2).order(ByteOrder.nativeOrder).asFloatBuffer
    result.put(array).flip
    result
  }

  def createIntBuffer(array: Array[Int]): IntBuffer = {
    val result = ByteBuffer.allocateDirect(array.length << 2).order(ByteOrder.nativeOrder).asIntBuffer
    result.put(array).flip
    result
  }
}
