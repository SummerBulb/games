package com.summerbulb.lwjgl

import game.graphics.VertexArray
import game.input.KeyboardInput
import org.lwjgl.glfw.GLFW._
import org.lwjgl.glfw.GLFWVidMode
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL20._
import org.lwjgl.system.MemoryUtil.NULL

import scala.collection.mutable


class OpenGLTest() extends Runnable {
  val width: Int = 1280
  val height: Int = 720

  var isRunning = false

  var window: Long = 0L

  val vertices = Array[Float](
    -0.5f, -0.5f, -0.5f,
    -0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, -0.5f,
    0.5f, -0.5f, -0.5f
  )

  val indices = Array[Byte](
    0, 1, 2,
    2, 3, 0

  )

  val textureCoordinates = Array[Float](
    0, 1,
    0, 0,
    1, 0,
    1, 1
  )

  val vertShader =
    "#version 400 core\n" +
    "\n" +
    "layout (location = 0) in vec4 position;\n" +
    "layout (location = 1) in vec2 tc;\n" +
    "\n" +
//    "uniform mat4 pr_matrix;\n" +
    "\n" +
    "void main()\n" +
    "{\n" +
    "    gl_Position = position;\n" +
    "}"

  val fragShader =
    "#version 400 core\n" +
    "\n" +
    "layout (location = 0) out vec4 color;\n" +
    "\n" +
    "void main()\n" +
    "{\n" +
    "    color = vec4(0.2, 0.3, 0.8, 1.0);\n" +
    "}"

  lazy val background = new VertexArray(vertices, indices, textureCoordinates)
  lazy val shaderProgramId = createShaderProgram(vertShader, fragShader)

  val cache = mutable.Map[String, Int]()

  def getUniform(name: String) = {

    if (cache.contains(name))
      cache(name)
    else {

      val location = glGetUniformLocation(shaderProgramId, name)
      if (location == -1) {
        System.err.println("Could not find uniform variable '" + name + "'!")
      } else {
        cache.put(name, location)
      }
      location
    }
  }

  def start() = {
    isRunning = true
    val thread = new Thread(this, "OpenGlTest")
    thread.start()
  }

  def init(): Unit = {
    if (!glfwInit()) {
      throw new Exception("Unable to initialize GLFW")
    }

    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3)
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)

    window = glfwCreateWindow(width, height, "OpenGlTest", NULL, NULL)

    if (window == null) {
      throw new Exception("Could not create window.")
    }

    val vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor())

    glfwSetWindowPos(window, (GLFWVidMode.WIDTH - width) / 2, (GLFWVidMode.HEIGHT - height) / 2)

    glfwSetKeyCallback(window, new KeyboardInput())

    glfwMakeContextCurrent(window)
    glfwShowWindow(window)
    GL.createCapabilities()


    glClearColor(1.0f, 1.0f, 1.0f, 1.0f)
    glEnable(GL_DEPTH_TEST)
    println("OpenGL: " + glGetString(GL_VERSION))
  }

  override def run(): Unit = {
    init()
    while (isRunning) {
      update()
      render()

      if (glfwWindowShouldClose(window))
        isRunning = false
    }
  }

  def update(): Unit = {
    glfwPollEvents()


    // ESC closes the window
    if (KeyboardInput.keys(GLFW_KEY_ESCAPE)) {
      isRunning = false
    }
  }

  def render(): Unit = {
//    glClear(GL_COLOR_BUFFER_BIT)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)


    glUseProgram(shaderProgramId)
    background.render()
    glUseProgram(0)

    glfwSwapBuffers(window)

  }

  def createShaderProgram(vert: String, frag: String): Int = {
    val program:Int = glCreateProgram()
    val vertID = glCreateShader(GL_VERTEX_SHADER)
    val fragID = glCreateShader(GL_FRAGMENT_SHADER)

    glShaderSource(vertID, vert)
    glShaderSource(fragID, frag)

    glCompileShader(vertID)
    if (glGetShaderi(vertID, GL_COMPILE_STATUS) != GL_TRUE) {
      val infoLog = glGetShaderInfoLog(vertID)
      throw new Exception("Failed to compile vertex shader.\nDetails:\n" + infoLog)
    }

    glCompileShader(fragID)
    if (glGetShaderi(fragID, GL_COMPILE_STATUS) != GL_TRUE) {
      val infoLog = glGetShaderInfoLog(fragID)
      throw new Exception("Failed to compile fragment shader.\nDetails:\n" + infoLog)
    }

    glAttachShader(program, vertID)
    glAttachShader(program, fragID)
    glLinkProgram(program)


    glValidateProgram(program)

    glDeleteShader(vertID)
    glDeleteShader(fragID)

    val error = glGetError()
    if (error != GL_NO_ERROR) {
      println(error)
      System.exit(200)
    }

    program
  }

}

object OpenGLTest {
  def main(args: Array[String]): Unit = {
    new OpenGLTest().start()
  }
}

