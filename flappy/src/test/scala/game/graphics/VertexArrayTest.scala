package game.graphics

import org.scalatest.FunSuite

class ConstructorOverloadTest extends FunSuite {

  test("Overload Parameter shadows class member") {
    val actual = new TestME(200).length

    assert(actual === 200)
    assert(actual !== 1)

    println(new TestME(200).length)
  }

  class TestME(name:String, len:Option[Int]=None) {
    def length = len getOrElse name.length

    def this(length:Int) = {
      this("*",Some(length))
    }


  }
}