package game.graphics

import org.scalatest.FunSuite

class MillisecVsNanoSecTest extends FunSuite {

  test("Nano-Second is 1/1000000 of a Milli-Second") {

    val miliStart = System.currentTimeMillis()
    val nanoStart = System.nanoTime()

    Thread.sleep(1000)

    val nanoEnd = System.nanoTime()
    val miliEnd = System.currentTimeMillis()

    val nanoLength: Float = nanoEnd - nanoStart
    val miliLegth: Float = miliEnd - miliStart

    println(s"Mili length: $miliLegth")
    println(s"Nano length: $nanoLength")
    println("*** " + miliLegth / nanoLength + " ***")

  }

  class TestME(name: String, len: Option[Int] = None) {
    def length = len getOrElse name.length

    def this(length: Int) = {
      this("*", Some(length))
    }


  }

}